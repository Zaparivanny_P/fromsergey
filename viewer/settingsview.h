#ifndef SETTINGSVIEW_H
#define SETTINGSVIEW_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLayout>

#include "model/setting.h"

class SettingsView : public QWidget
{
    Q_OBJECT
public:
    explicit SettingsView(Setting *sm, QWidget *parent = 0);
    ~SettingsView();
    QString param(int index);
private:
    QLineEdit *param1;
    QLineEdit *param2;
    QPushButton *btnSave;
    QVBoxLayout *vlay;

signals:
    void save();
public slots:
    void btnsave();
};

#endif // SETTINGSVIEW_H
