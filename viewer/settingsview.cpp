#include "settingsview.h"

SettingsView::SettingsView(Setting *sm, QWidget *parent) : QWidget(parent)
{
    param1 = new QLineEdit(sm->data(Setting::PARAM1).toString());
    param2 = new QLineEdit(sm->data(Setting::PARAM2).toString());
    btnSave = new QPushButton("Save");
    connect(btnSave, SIGNAL(released()), SLOT(btnsave()));
    vlay = new QVBoxLayout();
    vlay->addWidget(param1);
    vlay->addWidget(param2);
    vlay->addWidget(btnSave);
    setLayout(vlay);
}

SettingsView::~SettingsView()
{
    delete param1;
    delete param2;
    delete btnSave;
    delete vlay;
}

QString SettingsView::param(int index)
{
    switch(index)
    {
    case 1:
        return param1->text();
    case 2:
        return param2->text();
    default:
        return "";
    }
}


void SettingsView::btnsave()
{
    emit save();
}
