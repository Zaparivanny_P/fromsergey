#include "settingscontroller.h"

SettingsController::SettingsController(Setting *sm, QObject *parent) : sm(sm)
{

}

SettingsController::~SettingsController()
{

}

void SettingsController::setView(SettingsView *sv)
{
    this->sv = sv;
    connect(sv, SIGNAL(save()), SLOT(save()));
}

void SettingsController::save()
{
    if(sv != NULL)
    {
        sm->setData(Setting::PARAM1, sv->param(1));
        sm->setData(Setting::PARAM2, sv->param(2));
    }
    sm->save();
}
