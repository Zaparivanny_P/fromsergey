#ifndef SETTINGSCONTROLLER_H
#define SETTINGSCONTROLLER_H

#include <QObject>

#include "model/setting.h"
#include "viewer/settingsview.h"


class SettingsController : public QObject
{
    Q_OBJECT
public:
    explicit SettingsController(Setting *sm, QObject *parent = 0);
    ~SettingsController();
    void setView(SettingsView *sv);

private:
    Setting *sm;
    SettingsView *sv;

signals:

public slots:
    void save();
};

#endif // SETTINGSCONTROLLER_H
