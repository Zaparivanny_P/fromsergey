#include "setting.h"

Setting::Setting()
{
    QVariant tmp;
    stg = new QSettings("config.ini",
                        QSettings::IniFormat);
    stg->beginGroup("GLOBAL");
    tmp = stg->value("param1");
    mapConfig.insert(Setting::PARAM1, tmp);

    tmp = stg->value("param2");
    mapConfig.insert(Setting::PARAM2, tmp);
    stg->endGroup();
}

Setting::~Setting()
{
    delete stg;
}

void Setting::setData(Setting::SettingKey key, QVariant val)
{
    mapConfig.insert(key, val);
}

QVariant Setting::data(Setting::SettingKey key)
{
    return mapConfig.value(key);
}

void Setting::save()
{
    stg->beginGroup("GLOBAL");
    stg->setValue("param1", mapConfig.value(Setting::PARAM1));
    stg->setValue("param2", mapConfig.value(Setting::PARAM2));
    stg->endGroup();
}
