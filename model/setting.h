#ifndef SETTING_H
#define SETTING_H

#include <QVariant>
#include <QSettings>
#include <QMap>



class Setting
{
public:
    typedef enum SettingKey
    {
        PARAM1 = 1,
        PARAM2,
    }SettingKey;

    Setting();
    ~Setting();

    void setData(SettingKey key, QVariant val);
    QVariant data(SettingKey key);
    void save();
private:
    QSettings *stg;
    QMap<SettingKey, QVariant> mapConfig;
};

#endif // SETTING_H
