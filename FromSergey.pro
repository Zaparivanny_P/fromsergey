#-------------------------------------------------
#
# Project created by QtCreator 2016-02-22T23:40:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FromSergey
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    model/setting.cpp \
    controller/settingscontroller.cpp \
    viewer/settingsview.cpp

HEADERS  += mainwindow.h \
    model/setting.h \
    controller/settingscontroller.h \
    viewer/settingsview.h
