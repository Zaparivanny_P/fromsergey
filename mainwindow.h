#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "viewer/settingsview.h"
#include "model/setting.h"
#include "controller/settingscontroller.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    SettingsView *sv;
    Setting *sm;
    SettingsController *sc;
};

#endif // MAINWINDOW_H
