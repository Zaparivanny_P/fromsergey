#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    sm = new Setting();

    sc = new SettingsController(sm);
    sv = new SettingsView(sm);
    sc->setView(sv);

    setCentralWidget(sv);
}

MainWindow::~MainWindow()
{
    delete sm;
    delete sv;
}
